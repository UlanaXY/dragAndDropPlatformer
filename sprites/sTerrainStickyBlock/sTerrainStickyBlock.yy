{
    "id": "f36816bb-d2b2-4cd5-89de-4a78229d129c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTerrainStickyBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ee56b7a-780e-48dc-be8d-e98fafd04de2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f36816bb-d2b2-4cd5-89de-4a78229d129c",
            "compositeImage": {
                "id": "74f06a15-ee93-4010-a330-f699cc815044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ee56b7a-780e-48dc-be8d-e98fafd04de2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d9dc004-f7cf-4203-8c22-ab20fac81d2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ee56b7a-780e-48dc-be8d-e98fafd04de2",
                    "LayerId": "be89f468-fcce-4e2f-b482-b451715dcf10"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "be89f468-fcce-4e2f-b482-b451715dcf10",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f36816bb-d2b2-4cd5-89de-4a78229d129c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}