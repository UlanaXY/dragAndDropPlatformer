{
    "id": "a9113768-55f4-43d0-b190-19bf14b98359",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c261f418-2c08-4f42-9d6b-01a8b2344441",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9113768-55f4-43d0-b190-19bf14b98359",
            "compositeImage": {
                "id": "45fef974-f2c7-4cca-af8b-3b9255e5f7c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c261f418-2c08-4f42-9d6b-01a8b2344441",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8b7534b-fb92-4bce-a6d5-6c08b1d24940",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c261f418-2c08-4f42-9d6b-01a8b2344441",
                    "LayerId": "f95a30bd-bd38-41f8-8b15-3ce1d84fcc8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f95a30bd-bd38-41f8-8b15-3ce1d84fcc8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9113768-55f4-43d0-b190-19bf14b98359",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}