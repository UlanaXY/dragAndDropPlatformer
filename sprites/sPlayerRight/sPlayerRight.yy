{
    "id": "214080e7-d56b-4198-8d5f-554ff192d0b6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPlayerRight",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 60,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bead907-6cb0-4464-ba15-254b204b0074",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "214080e7-d56b-4198-8d5f-554ff192d0b6",
            "compositeImage": {
                "id": "5b18642a-8f85-40eb-8af2-e86799ffbb1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bead907-6cb0-4464-ba15-254b204b0074",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0139788-e651-4c97-8d91-677e755b444c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bead907-6cb0-4464-ba15-254b204b0074",
                    "LayerId": "28cf75fa-490c-4474-981f-bfcfacd8cf7e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "28cf75fa-490c-4474-981f-bfcfacd8cf7e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "214080e7-d56b-4198-8d5f-554ff192d0b6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}