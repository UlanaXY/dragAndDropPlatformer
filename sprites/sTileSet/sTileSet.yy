{
    "id": "fa551c8d-d54b-4fef-a8da-aa27c0ba3fee",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTileSet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d95f5693-a524-47a4-a9ca-412f3ec8cf14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fa551c8d-d54b-4fef-a8da-aa27c0ba3fee",
            "compositeImage": {
                "id": "42537160-59bc-49c5-a59f-364f38314a85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95f5693-a524-47a4-a9ca-412f3ec8cf14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dfa3717-6d33-4bf9-a274-3e2d13f314c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95f5693-a524-47a4-a9ca-412f3ec8cf14",
                    "LayerId": "fae0b970-26e1-43fa-a25f-5add7ee39c6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "fae0b970-26e1-43fa-a25f-5add7ee39c6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fa551c8d-d54b-4fef-a8da-aa27c0ba3fee",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 96
}