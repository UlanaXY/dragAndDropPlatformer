{
    "id": "ef500102-7b2e-4115-ae75-5843d7b90a5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 1,
    "bbox_right": 60,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1b346088-f19e-412c-b1db-16dba06be822",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ef500102-7b2e-4115-ae75-5843d7b90a5b",
            "compositeImage": {
                "id": "b40322f1-a473-4358-a0bc-27acaa074ffa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b346088-f19e-412c-b1db-16dba06be822",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e542abf-87d6-474f-9ef1-128d4231f810",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b346088-f19e-412c-b1db-16dba06be822",
                    "LayerId": "ec269a05-2f7d-431f-b110-b3748cd86ead"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ec269a05-2f7d-431f-b110-b3748cd86ead",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ef500102-7b2e-4115-ae75-5843d7b90a5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}