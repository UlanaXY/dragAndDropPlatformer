{
    "id": "a6ff0652-62bf-4603-bc77-51391c714ea8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTerrainCollisionBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b463caa6-6e61-4bae-9f3d-353a2f05fdb2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6ff0652-62bf-4603-bc77-51391c714ea8",
            "compositeImage": {
                "id": "8e375008-9daf-4821-8b55-75f260e3e61a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b463caa6-6e61-4bae-9f3d-353a2f05fdb2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b8853d8-dda0-458d-87c9-9ac22eaa9321",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b463caa6-6e61-4bae-9f3d-353a2f05fdb2",
                    "LayerId": "cb1dded9-c973-41a8-8856-41ec6c151896"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "cb1dded9-c973-41a8-8856-41ec6c151896",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6ff0652-62bf-4603-bc77-51391c714ea8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}