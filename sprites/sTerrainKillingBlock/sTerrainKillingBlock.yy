{
    "id": "fc904af8-71eb-414c-8dc8-98b9d3244913",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sTerrainKillingBlock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8be14eb9-df52-43d5-b43c-7566a1325d74",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc904af8-71eb-414c-8dc8-98b9d3244913",
            "compositeImage": {
                "id": "c6313b6d-1225-4bef-8431-96a04286c2a3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8be14eb9-df52-43d5-b43c-7566a1325d74",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1781153-a89e-4570-a116-f7a112286dd4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8be14eb9-df52-43d5-b43c-7566a1325d74",
                    "LayerId": "1f9792f5-129d-441f-982f-46344e3a0004"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1f9792f5-129d-441f-982f-46344e3a0004",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc904af8-71eb-414c-8dc8-98b9d3244913",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}