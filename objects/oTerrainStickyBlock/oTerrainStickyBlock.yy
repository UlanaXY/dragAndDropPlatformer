{
    "id": "8d3d1b40-e89b-4c7a-bf3c-09bc346300cb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTerrainStickyBlock",
    "eventList": [
        {
            "id": "da9faba7-2712-41a4-a657-c6588ed94487",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "8d3d1b40-e89b-4c7a-bf3c-09bc346300cb"
        },
        {
            "id": "305824c4-e316-4805-b8be-2a3ab43efd3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8d3d1b40-e89b-4c7a-bf3c-09bc346300cb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f36816bb-d2b2-4cd5-89de-4a78229d129c",
    "visible": true
}