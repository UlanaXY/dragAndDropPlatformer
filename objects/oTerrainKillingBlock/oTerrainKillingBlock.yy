{
    "id": "f2562504-1767-479e-a185-ae047d632a3c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTerrainKillingBlock",
    "eventList": [
        {
            "id": "0af6bb8b-d1fd-4486-9751-b50b4cdabea4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f2562504-1767-479e-a185-ae047d632a3c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fc904af8-71eb-414c-8dc8-98b9d3244913",
    "visible": true
}