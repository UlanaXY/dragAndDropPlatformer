{
    "id": "8af6c94a-a549-4df6-b791-728e2a6b2c79",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oPlayer",
    "eventList": [
        {
            "id": "e1a216f8-39b2-4b74-b50c-4016e472e646",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8af6c94a-a549-4df6-b791-728e2a6b2c79"
        },
        {
            "id": "241487ab-85e1-4d8b-9327-f149d8187aa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8af6c94a-a549-4df6-b791-728e2a6b2c79"
        },
        {
            "id": "ca74f977-ca3a-4875-b68f-7d0370e0265e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "18a4fb57-136d-41a1-9fec-0a814b24558b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8af6c94a-a549-4df6-b791-728e2a6b2c79"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "2a916fee-a674-430d-8ee9-ce7a259ce6f6",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "be51049c-1948-4be4-9268-9689e0dfda59",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "6bf8328f-84f9-4b07-9738-ceb8fdc30513",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "6f93b4a2-cce6-42d7-9785-259cee982d97",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ef500102-7b2e-4115-ae75-5843d7b90a5b",
    "visible": true
}