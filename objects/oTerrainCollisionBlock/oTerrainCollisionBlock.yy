{
    "id": "0e3318c7-b8f2-4ac8-9f63-19430c57ddaa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oTerrainCollisionBlock",
    "eventList": [
        {
            "id": "68300144-f2f4-4b94-8bfc-4f20c1838dd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0e3318c7-b8f2-4ac8-9f63-19430c57ddaa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "18a4fb57-136d-41a1-9fec-0a814b24558b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a6ff0652-62bf-4603-bc77-51391c714ea8",
    "visible": true
}