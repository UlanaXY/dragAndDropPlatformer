var tilesId = layer_get_id("Tiles_1");

if keyboard_check(vk_left) {
    layer_x(tilesId, layer_get_x(tilesId) + 5);
}

if keyboard_check(vk_right) {
    layer_x(tilesId, layer_get_x(tilesId) - 5);
}
